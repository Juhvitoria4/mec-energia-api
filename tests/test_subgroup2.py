import pytest
from utils.subgroup_util import Subgroup  

def test_voltagem_menor_que_0():
    with pytest.raises(Exception, match='Subgroup not found for this supply voltage'):
        Subgroup.get_subgroup(-1)


def test_voltagem_intervalo_subgrupo_as():
    resultado = Subgroup.get_subgroup(1.5)
    assert resultado == Subgroup.AS

def test_voltagem_intervalo_subgrupo_a4():
    resultado = Subgroup.get_subgroup(15)
    assert resultado == Subgroup.A4

def test_voltagem_intervalo_subgrupo_a3a():
    resultado = Subgroup.get_subgroup(35)
    assert resultado == Subgroup.A3A

def test_voltagem_igual_subgrupo_a3():
    resultado = Subgroup.get_subgroup(69)
    assert resultado == Subgroup.A3

def test_voltagem_intervalo_subgrupo_a2():
    resultado = Subgroup.get_subgroup(100)
    assert resultado == Subgroup.A2

def test_voltagem_maior_que_138():
    with pytest.raises(Exception, match='Subgroup not found for this supply voltage'):
        Subgroup.get_subgroup(150)


