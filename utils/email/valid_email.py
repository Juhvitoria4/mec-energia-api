import re
import requests
import pytest

def verify_email_is_valid(email, during_registration=False):
email_lower = email.lower()
regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'

if not (re.search(regex, email_lower)):
raise Exception('Email not valid')

if during_registration:
# Durante o cadastro, agora aceitamos variações de caixa alta/baixa
if not re.search(regex, email_lower):
raise Exception('Invalid email format during registration')

""" endpoint = 'https://www.verifyemailaddress.org/email-validation/result/'
data = {
'email': email_lower
}

response = requests.post(endpoint, data)
"""

return True

@pytest.mark.django_db

class Test:

def test_login_with_uppercase_email(self):
# Teste para simular login com e-mail em letras maiúsculas
uppercase_email = 'USER@EXAMPLE.COM'
# Não deve levantar exceção, pois agora aceitamos variações de caixa alta/baixa
verify_email_is_valid(uppercase_email)

def test_registration_with_capslock_email(self):
# Teste para simular cadastro com o CapsLock ativado
capslock_email = 'USER@EXAMPLE.COM'
# Não deve levantar exceção, pois agora aceitamos variações de caixa alta/baixa durante o cadastro
verify_email_is_valid(capslock_email, during_registration=True)
def test_invalid_email_format(self):
# Teste para um formato de e-mail inválido
invalid_email = 'invalidemail'
with pytest.raises(Exception, match='Invalid email format during registration'):
verify_email_is_valid(invalid_email)

